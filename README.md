﻿# README #

This library is intended to provide the most minimal portable view model possible, providing only a basic property notifier class with simple methods to generate those notifications.

This library literally contains a single BaseViewModel class and that's it. It is recommended that you also use Acrotech.PortableDelegateCommand to provide commanding support.

### What is this repository for? ###

* Anyone who is writing a cross platform application and wants to use MVVM but does not want to bring in any heavy view model architecture.

### How do I get set up? ###

0. Include this library
0. Subclass the BaseViewModel
0. Use RaiseAndSetIfChanged in your property setters

### Who do I talk to? ###

* Contact me for suggestions, improvements, or bugs

### Changelog ###

#### 1.0.1.0 ####

* Added Acrotech.Versioning for versioning
* TaskBaseViewModel can now return "Empty Completed" Tasks instead of null
* Improved snippets
* Improved OnPropertyChanged function chaining (more options, allows args creation delegates)
* Refactored main RaiseAndSetIfChanged logic to extension methods (supports using extensions with another INotifyPropertyChanged view model framework)
* Usings cleanup
* Fully unit tested!
* Adding CF35 support
* Updating PCL profile to 344 (maximum compatibility)
* Improved the task scheduling version of the base view model (now allows scheduling tasks with results)
* Added nullable OnChanging delegate to RaiseAndSetIfChanged calls (called before assignment and event generation)
* adding delay and cancellation token options to threading functions

#### 1.0.0.3 ####

* Fixing bug with deprecated RaiseAndSetIfChanged where a null onChanged delegate would cause a NullReferenceException
    * Also fixing an ambiguous call compile time issue between the standard and deprecated functions

#### 1.0.0.2 ####

* Adding the TaskBaseViewModel, which supports the TaskScheduler to invoke property changes (or other delegates) on the UI thread
    * Note that this may not fully work properly yet, this is experimental at the moment
* Source code now contains Snippets for creating view model properties (still need to figure out how to include this via NuGet, so if you want to use them get them at the source repository)
    * You can place the snippets in %USERPROFILE%\Documents\Visual Studio 2012\Code Snippets\Visual C#\My Code Snippets

#### 1.0.0.1 ####

* On Changed delegate of RaiseAndSetIfChanged now provides the old property value as a delegate parameter (no parameter is still allowed for compatibility purposes)

#### 1.0.0.0 ####

* Initial Release
