﻿<#@ template debug="false" hostSpecific="true" #>
<#@ output extension="g.cs" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ import namespace="System.IO" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.Text.RegularExpressions" #>
<#

// Transform this template by running: "%CommonProgramFiles(x86)%\microsoft shared\TextTemplating\11.0\TextTransform.exe" [Options] VersionDetails.tt
// Supply template parameters as options with -a !!ParameterName!ParameterValue (i.e., -a !!ManualBuild!True )

// Supported Template Parameters
// * ManualBuild: Assign to anything to transform as a manual build (no -DEV suffix)

var versionProperties = Parse(@"version.properties");
var buildProperties = Parse(@"build.properties");

#>
using Acrotech.PortableViewModel.Properties;
using System;
using System.Reflection;
using System.Text;

[assembly: AssemblyVersion(VersionDetails.ShortVersionString)]
[assembly: AssemblyFileVersion(VersionDetails.ShortVersionString)]
[assembly: AssemblyInformationalVersion(VersionDetails.LongVersionString)]

// TODO: update this namespace to match your project namespace (optionally keep Properties at the end)
namespace Acrotech.PortableViewModel.Properties
{
	/// <summary>
	/// This class contains version and build details
	/// </summary>
	public static partial class VersionDetails
	{
		/// <summary>
		/// Major version number
		/// </summary>
		public const string Major = "<#= GetValueOrDefault(versionProperties, "Major", "0") #>";
		/// <summary>
		/// Minor version number
		/// </summary>
		public const string Minor = "<#= GetValueOrDefault(versionProperties, "Minor", "0") #>";
		/// <summary>
		/// Release version number
		/// </summary>
		public const string Release = "<#= GetValueOrDefault(versionProperties, "Release", "0") #>";
		/// <summary>
		/// Revision version number
		/// </summary>
		public const string Revision = "<#= GetValueOrDefault(versionProperties, "Revision", "0") #>";
		/// <summary>
		/// Release candidate tag (if any)
		/// </summary>
		public const string ReleaseCandidate = "<#= GetValueOrDefault(versionProperties, "ReleaseCandidate", "") #>";

		/// <summary>
		/// Branch used to build this assembly (if any)
		/// </summary>
		public const string Branch = "<#= GetValueOrDefault(buildProperties, "Branch", "") #>";
		/// <summary>
		/// Commit ID used to build this assembly (if any)
		/// </summary>
		public const string Commit = "<#= GetValueOrDefault(buildProperties, "Commit", "") #>";
		/// <summary>
		/// Build number associated with this assembly (if any)
		/// </summary>
		public const string BuildNumber = "<#= GetValueOrDefault(buildProperties, "BuildNumber", "") #>";
		/// <summary>
		/// (Timestamp) Ticks at build time
		/// </summary>
		public const long BuildUtcTicks = <#= DateTime.UtcNow.Ticks #>;

		// these have to be consts so we can dump them into the Assembly globals

		/// <summary>
		/// Short Version (i.e. 1.0.0.0)
		/// </summary>
		public const string ShortVersionString = Major + "." + Minor + "." + Release + "." + Revision;
		/// <summary>
		/// Long Version (i.e. 1.0.0.0-develop-RC1-B22)
		/// </summary>
		public const string LongVersionString = ShortVersionString + "<#= GetLongVersionDetails(versionProperties, buildProperties) #>";

		/// <summary>
		/// True if built in debug mode, false otherwise
		/// </summary>
		public static bool IsDebugBuild
		{
			get
			{ 
				return 
	#if DEBUG
					true;
	#else
					false;
	#endif
			}
		}
	
		/// <summary>
		/// True if built on the master branch, false otherwise
		/// </summary>
		public static bool IsMasterBuild
		{
			get { return Branch == "master"; }
		}

		/// <summary>
		/// Version representation of the ShortVersion
		/// </summary>
		public static Version Version
		{
			get { return new Version(ShortVersionString); }
		}

		/// <summary>
		/// Displays ShortVersion when built on the master branch, otherwise LongVersion
		/// </summary>
		public static string DisplayVersionString
		{
			get { return IsMasterBuild ? ShortVersionString : LongVersionString; }
		}

		/// <summary>
		/// Build timestamp as a DateTimeOffset representation of the BuildUtcTicks
		/// </summary>
		public static DateTimeOffset BuildTimestamp
		{
			get
			{
				return new DateTimeOffset(BuildUtcTicks, TimeSpan.Zero);
			}
		}

		/// <summary>
		/// Unique build code composed of the timestamp and commit ID
		/// </summary>
		/// <remarks>
		/// Format is yyDOY.HHmmss[-commitID] 
		/// * DOY is day of year
		/// * commitID only included if present during build
		/// </remarks>
		public static string BuildCode
		{
			get
			{
				var sb = new StringBuilder().AppendFormat("{0:yy}{1:000}.{0:HHmmss}", BuildTimestamp, BuildTimestamp.DayOfYear);

				if (string.IsNullOrEmpty(Commit) == false)
				{
					sb.AppendFormat("-{0}", Commit.Substring(0, 7));
				}

				return sb.ToString();
			}
		}
	}
}
<#+

public string ResolveParameterValue(string parameterName, string defaultValue = default(string))
{
	var value = defaultValue;

	try
	{
		value = Host.ResolveParameterValue(null, null, parameterName);
	}
	catch
	{
		// do nothing
	}

	return value;
}

const string KeyValuePairSplitter = "=";

static readonly char[] KeyValuePairSplitters = new char[] { KeyValuePairSplitter[0] };

public IDictionary<string, string> Parse(string path)
{
	Dictionary<string, string> properties = new Dictionary<string, string>();
	
	try
	{
		if (string.IsNullOrEmpty(path) == false)
		{
			FileInfo file = new FileInfo(Host.ResolvePath(path));

			if (file.Exists)
			{
				using (Stream stream = file.OpenRead())
				using (StreamReader sr = new StreamReader(stream))
				{
					string line = null;
					
					while (string.IsNullOrEmpty(line = sr.ReadLine()) == false)
					{
						if (line.TrimStart().StartsWith("#"))
						{
							continue;
						}
						
						string[] pair = line.Split(KeyValuePairSplitters);
						
						if (pair.Length >= 2)
						{
							string key = pair[0].Trim();
							string value = string.Join(KeyValuePairSplitter, pair, 1, pair.Length - 1);

							if (value.Length > 0 && value[0] == ' ')
							{
								value = value.Substring(1);
							}
							
							if (properties.ContainsKey(key))
							{
								properties[key] = value;
							}
							else	
							{
								properties.Add(key, value);
							}
						}
					}
				}
			}
		}
	}
	catch
	{
		// do nothing
	}
	
	return properties;
}

public string GetValueOrDefault(IDictionary<string, string> properties, string key, string defaultValue = null)
{
	string value = defaultValue;
	
	if (properties != null && properties.ContainsKey(key))
	{
		value = properties[key];
	}
	
	return value;
}

public string GetLongVersionDetails(IDictionary<string, string> versionProperties, IDictionary<string, string> buildProperties)
{
	var sb = new StringBuilder();

	var branch = GetValueOrDefault(buildProperties, "Branch", "");
	var releaseCandidate = GetValueOrDefault(versionProperties, "ReleaseCandidate", "");
	var buildNumber = GetValueOrDefault(buildProperties, "BuildNumber", "");

	if (string.IsNullOrEmpty(branch) == false)
	{
		sb.AppendFormat("-{0}", branch);
	}

	if (string.IsNullOrEmpty(releaseCandidate) == false)
	{
		sb.AppendFormat("-{0}", releaseCandidate);
	}

	if (string.IsNullOrEmpty(ResolveParameterValue("ManualBuild")))
	{
		sb.AppendFormat("-{0}", string.IsNullOrEmpty(buildNumber) ? "DEV" : string.Format("B{0}", buildNumber));
	}

	return sb.ToString();
}

#>
