﻿using System;
using System.ComponentModel;

namespace Acrotech.PortableViewModel
{
    /// <summary>
    /// Simple Property Notifier Base View Model
    /// </summary>
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        /// <inheritdoc/>
        public event PropertyChangedEventHandler PropertyChanged = null;

        #region OnPropertyChanged

        /// <summary>
        /// Invokes a property change notification for the provided <paramref name="propertyName"/>
        /// </summary>
        /// <param name="propertyName">Name of property that changed</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(PropertyChanged, () => new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Invokes a property change notification with the provided <paramref name="args"/>
        /// </summary>
        /// <param name="args">Property change args</param>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            OnPropertyChanged(PropertyChanged, () => args);
        }

        /// <summary>
        /// Invokes a property change notification with the provided <paramref name="argsCreator"/> only if the event is handled
        /// </summary>
        /// <param name="argsCreator">Property change args (creation delegate)</param>
        protected virtual void OnPropertyChanged(Func<PropertyChangedEventArgs> argsCreator)
        {
            OnPropertyChanged(PropertyChanged, argsCreator);
        }

        /// <summary>
        /// Invokes a property change notification using the provided <paramref name="handler"/> and <paramref name="argsCreator"/> only if <paramref name="handler"/> is non-null
        /// </summary>
        /// <param name="handler">Notification handler to invoke</param>
        /// <param name="argsCreator">Property change args (creation delegate)</param>
        protected virtual void OnPropertyChanged(PropertyChangedEventHandler handler, Func<PropertyChangedEventArgs> argsCreator)
        {
            if (handler != null)
            {
                OnPropertyChanged(handler, argsCreator());
            }
        }

        /// <summary>
        /// Invokes a property change notification using the provided <paramref name="handler"/> and <paramref name="args"/> only if <paramref name="handler"/> is non-null
        /// </summary>
        /// <param name="handler">Notification handler to invoke</param>
        /// <param name="args">Property change args</param>
        /// <remarks>This is the final function call before the notification is invoked</remarks>
        protected virtual void OnPropertyChanged(PropertyChangedEventHandler handler, PropertyChangedEventArgs args)
        {
            if (handler != null)
            {
                handler(this, args);
            }
        }

        #endregion

        #region RaiseAndSetIfChanged

#if PocketPC
        protected virtual T RaiseAndSetIfChanged<T>(ref T backingField, T newValue, string propertyName)
        {
            return RaiseAndSetIfChanged(ref backingField, newValue, propertyName, (Action<T>)null, (Action<T>)null);
        }

        protected virtual T RaiseAndSetIfChanged<T>(ref T backingField, T newValue, PropertyChangedEventArgs args)
        {
            return RaiseAndSetIfChanged(ref backingField, newValue, args, (Action<T>)null, (Action<T>)null);
        }

        protected virtual T RaiseAndSetIfChanged<T>(ref T backingField, T newValue, Func<PropertyChangedEventArgs> argsCreator)
        {
            return RaiseAndSetIfChanged(ref backingField, newValue, argsCreator, (Action<T>)null, (Action<T>)null);
        }
#endif

        /// <summary>
        /// Helper function to simplify property changes and subsequent notifications by combining all code execution into a single function call. Automatic value equality comparison and dynamic change notification are built-in.
        /// </summary>
        /// <typeparam name="T">Changed property type</typeparam>
        /// <param name="backingField">Reference to the changed property backing field</param>
        /// <param name="newValue">Changed property's new value</param>
        /// <param name="propertyName">Name of the property</param>
        /// <param name="onChanged">On Changed delegate to execute</param>
        /// <param name="onChanging">On Changing delegate to execute (before assignment)</param>
        /// <returns>The resulting value of the property</returns>
        /// <remarks>This function calls the equivalent extension method</remarks>
#if PocketPC
        protected virtual T RaiseAndSetIfChanged<T>(ref T backingField, T newValue, string propertyName, Action<T> onChanged, Action<T> onChanging)
#else
        protected virtual T RaiseAndSetIfChanged<T>(ref T backingField, T newValue, string propertyName, Action<T> onChanged = null, Action<T> onChanging = null)
#endif
        {
            return this.RaiseAndSetIfChanged(PropertyChanged, ref backingField, newValue, propertyName, onChanged, onChanging);
        }

        /// <summary>
        /// Helper function to simplify property changes and subsequent notifications by combining all code execution into a single function call. Automatic value equality comparison and dynamic change notification are built-in.
        /// </summary>
        /// <typeparam name="T">Changed property type</typeparam>
        /// <param name="backingField">Reference to the changed property backing field</param>
        /// <param name="newValue">Changed property's new value</param>
        /// <param name="args">Property change args</param>
        /// <param name="onChanged">On Changed delegate to execute</param>
        /// <param name="onChanging">On Changing delegate to execute (before assignment)</param>
        /// <returns>The resulting value of the property</returns>
        /// <remarks>This function calls the equivalent extension method</remarks>
#if PocketPC
        protected virtual T RaiseAndSetIfChanged<T>(ref T backingField, T newValue, PropertyChangedEventArgs args, Action<T> onChanged, Action<T> onChanging)
#else
        protected virtual T RaiseAndSetIfChanged<T>(ref T backingField, T newValue, PropertyChangedEventArgs args, Action<T> onChanged = null, Action<T> onChanging = null)
#endif
        {
            return this.RaiseAndSetIfChanged(PropertyChanged, ref backingField, newValue, args, onChanged, onChanging);
        }

        /// <summary>
        /// Helper function to simplify property changes and subsequent notifications by combining all code execution into a single function call. Automatic value equality comparison and dynamic change notification are built-in.
        /// </summary>
        /// <typeparam name="T">Changed property type</typeparam>
        /// <param name="backingField">Reference to the changed property backing field</param>
        /// <param name="newValue">Changed property's new value</param>
        /// <param name="argsCreator">Delegate to create the property change args</param>
        /// <param name="onChanged">On Changed delegate to execute</param>
        /// <param name="onChanging">On Changing delegate to execute (before assignment)</param>
        /// <returns>The resulting value of the property</returns>
        /// <remarks>This function calls the equivalent extension method</remarks>
#if PocketPC
        protected virtual T RaiseAndSetIfChanged<T>(ref T backingField, T newValue, Func<PropertyChangedEventArgs> argsCreator, Action<T> onChanged, Action<T> onChanging)
#else
        protected virtual T RaiseAndSetIfChanged<T>(ref T backingField, T newValue, Func<PropertyChangedEventArgs> argsCreator, Action<T> onChanged = null, Action<T> onChanging = null)
#endif
        {
            return this.RaiseAndSetIfChanged(PropertyChanged, ref backingField, newValue, argsCreator, onChanged, onChanging);
        }

        /// <summary>
        /// Helper function to simplify property changes and subsequent notifications by combining all code execution into a single function call. Automatic value equality comparison and dynamic change notification are built-in.
        /// </summary>
        /// <typeparam name="T">Changed property type</typeparam>
        /// <param name="backingField">Reference to the changed property backing field</param>
        /// <param name="newValue">Changed property's new value</param>
        /// <param name="propertyName">Name of the property</param>
        /// <param name="onChanged">On Changed delegate to execute</param>
        /// <returns>The resulting value of the property</returns>
        /// <remarks>This function calls the equivalent extension method</remarks>
        [Obsolete("Please use the onChanged delegate that provides the old property value instead", false)]
        protected virtual T RaiseAndSetIfChanged<T>(ref T backingField, T newValue, string propertyName, Action onChanged)
        {
            return this.RaiseAndSetIfChanged(PropertyChanged, ref backingField, newValue, propertyName, onChanged);
        }

        #endregion
    }
}
