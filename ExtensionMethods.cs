﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace Acrotech.PortableViewModel
{
    /// <summary>
    /// Extension methods to support performing a RaiseAndSetIfChanged call on any view model framework that implements INotifyPropertyChanged
    /// </summary>
    public static partial class ExtensionMethods
    {
        #region RaiseAndSetIfChanged

#if PocketPC
        public static T RaiseAndSetIfChanged<T>(this INotifyPropertyChanged source, PropertyChangedEventHandler handler, ref T backingField, T newValue, string propertyName)
        {
            return RaiseAndSetIfChanged(source, handler, ref backingField, newValue, propertyName, (Action<T>)null, (Action<T>)null);
        }

        public static T RaiseAndSetIfChanged<T>(this INotifyPropertyChanged source, PropertyChangedEventHandler handler, ref T backingField, T newValue, PropertyChangedEventArgs args)
        {
            return RaiseAndSetIfChanged(source, handler, ref backingField, newValue, args, (Action<T>)null, (Action<T>)null);
        }

        public static T RaiseAndSetIfChanged<T>(this INotifyPropertyChanged source, PropertyChangedEventHandler handler, ref T backingField, T newValue, Func<PropertyChangedEventArgs> argsCreator)
        {
            return RaiseAndSetIfChanged(source, handler, ref backingField, newValue, argsCreator, (Action<T>)null, (Action<T>)null);
        }
#endif

        /// <summary>
        /// Helper function to simplify property changes and subsequent notifications by combining all code execution into a single function call. Automatic value equality comparison and dynamic change notification are built-in.
        /// </summary>
        /// <typeparam name="T">Changed property type</typeparam>
        /// <param name="source">View model that generated the property change notification</param>
        /// <param name="handler">Event handler that will produce the notification</param>
        /// <param name="backingField">Reference to the changed property backing field</param>
        /// <param name="newValue">Changed property's new value</param>
        /// <param name="propertyName">Name of the property</param>
        /// <param name="onChanged">On Changed delegate to execute</param>
        /// <param name="onChanging">On Changing delegate to execute (before assignment)</param>
        /// <returns>The resulting value of the property</returns>
#if PocketPC
        public static T RaiseAndSetIfChanged<T>(this INotifyPropertyChanged source, PropertyChangedEventHandler handler, ref T backingField, T newValue, string propertyName, Action<T> onChanged, Action<T> onChanging)
#else
        public static T RaiseAndSetIfChanged<T>(this INotifyPropertyChanged source, PropertyChangedEventHandler handler, ref T backingField, T newValue, string propertyName, Action<T> onChanged = null, Action<T> onChanging = null)
#endif
        {
            return RaiseAndSetIfChanged(source, handler, ref backingField, newValue, () => new PropertyChangedEventArgs(propertyName), onChanged, onChanging);
        }

        /// <summary>
        /// Helper function to simplify property changes and subsequent notifications by combining all code execution into a single function call. Automatic value equality comparison and dynamic change notification are built-in.
        /// </summary>
        /// <typeparam name="T">Changed property type</typeparam>
        /// <param name="source">View model that generated the property change notification</param>
        /// <param name="handler">Event handler that will produce the notification</param>
        /// <param name="backingField">Reference to the changed property backing field</param>
        /// <param name="newValue">Changed property's new value</param>
        /// <param name="args">Property change args</param>
        /// <param name="onChanged">On Changed delegate to execute</param>
        /// <param name="onChanging">On Changing delegate to execute (before assignment)</param>
        /// <returns>The resulting value of the property</returns>
#if PocketPC
        public static T RaiseAndSetIfChanged<T>(this INotifyPropertyChanged source, PropertyChangedEventHandler handler, ref T backingField, T newValue, PropertyChangedEventArgs args, Action<T> onChanged, Action<T> onChanging)
#else
        public static T RaiseAndSetIfChanged<T>(this INotifyPropertyChanged source, PropertyChangedEventHandler handler, ref T backingField, T newValue, PropertyChangedEventArgs args, Action<T> onChanged = null, Action<T> onChanging = null)
#endif
        {
            return RaiseAndSetIfChanged(source, handler, ref backingField, newValue, () => args, onChanged, onChanging);
        }

        /// <summary>
        /// Helper function to simplify property changes and subsequent notifications by combining all code execution into a single function call. Automatic value equality comparison and dynamic change notification are built-in.
        /// </summary>
        /// <typeparam name="T">Changed property type</typeparam>
        /// <param name="source">View model that generated the property change notification</param>
        /// <param name="handler">Event handler that will produce the notification</param>
        /// <param name="backingField">Reference to the changed property backing field</param>
        /// <param name="newValue">Changed property's new value</param>
        /// <param name="argsCreator">Delegate to create the property change args</param>
        /// <param name="onChanged">On Changed delegate to execute</param>
        /// <param name="onChanging">On Changing delegate to execute (before assignment)</param>
        /// <returns>The resulting value of the property</returns>
#if PocketPC
        public static T RaiseAndSetIfChanged<T>(this INotifyPropertyChanged source, PropertyChangedEventHandler handler, ref T backingField, T newValue, Func<PropertyChangedEventArgs> argsCreator, Action<T> onChanged, Action<T> onChanging)
#else
        public static T RaiseAndSetIfChanged<T>(this INotifyPropertyChanged source, PropertyChangedEventHandler handler, ref T backingField, T newValue, Func<PropertyChangedEventArgs> argsCreator, Action<T> onChanged = null, Action<T> onChanging = null)
#endif
        {
            if (source != null && EqualityComparer<T>.Default.Equals(backingField, newValue) == false)
            {
                if (onChanging != null)
                {
                    onChanging(newValue);
                }

                var oldValue = backingField;

                backingField = newValue;

                if (handler != null)
                {
                    handler(source, argsCreator == null ? null : argsCreator());
                }

                if (onChanged != null)
                {
                    onChanged(oldValue);
                }
            }

            return newValue;
        }

        /// <summary>
        /// Helper function to simplify property changes and subsequent notifications by combining all code execution into a single function call. Automatic value equality comparison and dynamic change notification are built-in.
        /// </summary>
        /// <typeparam name="T">Changed property type</typeparam>
        /// <param name="source">View model that generated the property change notification</param>
        /// <param name="handler">Event handler that will produce the notification</param>
        /// <param name="backingField">Reference to the changed property backing field</param>
        /// <param name="newValue">Changed property's new value</param>
        /// <param name="propertyName">Name of the property</param>
        /// <param name="onChanged">On Changed delegate to execute</param>
        /// <returns>The resulting value of the property</returns>
        [Obsolete("Please use the onChanged delegate that provides the old property value instead", false)]
        public static T RaiseAndSetIfChanged<T>(this INotifyPropertyChanged source, PropertyChangedEventHandler handler, ref T backingField, T newValue, string propertyName, Action onChanged)
        {
            return RaiseAndSetIfChanged(source, handler, ref backingField, newValue, propertyName, onChanged == null ? (Action<T>)null : _ => onChanged());
        }

        #endregion
    }
}
